import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import PatientForm from "./patient-form";

import * as API_PATIENTS from "./api/patient-api"

const columns = [
    {
        Header:  'patient_id',
        accessor: 'patient_id',
    },
    {
        Header: 'address',
        accessor: 'address',
    },
    {
        Header: 'birthdate',
        accessor: 'birthdate',
    },
    {
        Header: 'gender',
        accessor: 'gender',
    },
    {
        Header: 'medicalrecord',
        accessor: 'medicalrecord',
    },
    {
        Header: 'name',
        accessor: 'name',
    },
    {
        Header: 'caregiver_id',
        accessor: 'caregiver_id',
    },
    {
        Header: 'user_id',
        accessor: 'user_id',
    },
];

const filters = [];

class Patients extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        patient_id: x.patient_id,
                        address: x.address,
                        birthdate: x.birthdate,
                        gender: x.gender,
                        medicalrecord: x.medicalrecord,
                        name: x.name,
                        caregiver_id: x.caregiver_id,
                        user_id: x.user_id,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <PatientForm
                                    registerPatient={this.refresh}
                                    updatePatient={this.refresh}>
                                </PatientForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Patients;
