package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.MedicationsDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.MedicationsBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Medications;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationsRepository;
import com.example.springdemo.services.MedicationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationsService {
    private final MedicationsRepository medicationsRepository;

    @Autowired
    public MedicationsService(MedicationsRepository medicationsRepository){
        this.medicationsRepository = medicationsRepository;
    }

    public MedicationsDTO findMedicationsById(Integer id){

        Optional<Medications> medications  = medicationsRepository.findById(id);

        if (!medications.isPresent()) {
            throw new ResourceNotFoundException("Medications", "medications id", id);
        }
        return MedicationsBuilder.generateDTOFromEntity(medications.get());
    }

    public List<MedicationsDTO> findAll(){
        List<Medications> medicationsList = medicationsRepository.getAllOrdered();

        return medicationsList.stream()
                .map(MedicationsBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationsDTO medicationsDTO) {

        return medicationsRepository
                .save(MedicationsBuilder.generateEntityFromDTO(medicationsDTO))
                .getMedications_id();
    }

    public Integer update(MedicationsDTO medicationsDTO) {

        Optional<Medications> medications = medicationsRepository.findById(medicationsDTO.getMedications_id());

        if(!medications.isPresent()){
            throw new ResourceNotFoundException("Medications", "medications id", medicationsDTO.getMedications_id().toString());
        }

        return medicationsRepository.save(MedicationsBuilder.generateEntityFromDTO(medicationsDTO)).getMedications_id();
    }

    public void delete(MedicationsDTO medicationsDTO){
        this.medicationsRepository.deleteById(medicationsDTO.getMedications_id());
    }

}
