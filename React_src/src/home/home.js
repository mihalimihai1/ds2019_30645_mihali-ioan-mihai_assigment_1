import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {


    render() {
    console.log(localStorage.getItem('role'));

        return (
           <div>
                {
                    localStorage.getItem('role') === "doctor" ?
                        <div>
                            <Button color="primary" onClick={() => window.open('http://localhost:3000/caregiver')}>
                                Caregiver
                            </Button>

                            <Button color="primary" onClick={() => window.open('http://localhost:3000/patient')}>
                                Patient
                            </Button>

                            <Button color="primary" onClick={() => window.open('http://localhost:3000/intake')}>
                                Intake
                            </Button>

                            <Button color="primary" onClick={() => window.open('http://localhost:3000/medications')}>
                                Medication
                            </Button>
                        </div>:
                        localStorage.getItem('role') === "caregiver" ?
                                <div>
                                    <Button color="primary" onClick={() => window.open('http://localhost:3000/caregiverrole')}>
                                        Caregiver Role
                                    </Button>
                                </div> :
                            localStorage.getItem('role') === "patient" ?
                                window.alert("Hello patient!")
                                : null

                }
           </div>





        )
    };
}

export default Home
