package com.example.springdemo.controller;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value="/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService){
        this.caregiverService = caregiverService;
    }

    @GetMapping(value ="/find/{id}")
    public CaregiverDTO findById(@PathVariable("id") Integer id){
        return caregiverService.findCaregiverById(id);
    }

    @GetMapping()
    public List<CaregiverDTO> findAll(){
        return caregiverService.findAll();
        }

    @GetMapping(value = "/{idCaregiver}")
    public List<PatientDTO> findPatientsByCaregiver_id(@PathVariable("idCaregiver") Integer idCaregiver){
        return caregiverService.findPatientsByCaregiver_id(idCaregiver);
    }

        @PostMapping()
                public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
            return caregiverService.insert(caregiverDTO);
        }

        @PutMapping()
                public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO){
            return caregiverService.update(caregiverDTO);
        }

        @DeleteMapping()
        public void delete(@RequestBody CaregiverDTO caregiverDTO){
            caregiverService.delete(caregiverDTO);
        }
}
