package com.example.springdemo.controller;

import com.example.springdemo.dto.MedicationsDTO;
import com.example.springdemo.services.MedicationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value="/medications")
public class MedicationsController {

    private final MedicationsService medicationsService;

    @Autowired
    public MedicationsController(MedicationsService medicationsService) {
        this.medicationsService = medicationsService;
    }

    @GetMapping(value ="/{id}")
    public MedicationsDTO findById(@PathVariable("id") Integer id){
        return medicationsService.findMedicationsById(id);
    }

    @GetMapping()
    public List<MedicationsDTO> findAll(){
        return medicationsService.findAll();
    }

    @PostMapping()
    public Integer insertMedicationsDTO(@RequestBody MedicationsDTO medicationsDTO){
        return medicationsService.insert(medicationsDTO);
    }

    @PutMapping()
    public Integer updateMedications(@RequestBody MedicationsDTO medicationsDTO){
        return medicationsService.update(medicationsDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody MedicationsDTO medicationsDTO){
        medicationsService.delete(medicationsDTO);
    }


}
