import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_caregiver: '/caregiver/',
    post_caregiver: "/caregiver/",
    delete_caregiver: "/caregiver/",
    update_caregiver: "/caregiver"
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregiver, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

function getCaregiverById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_caregiver + params.id, {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.update_caregiver , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });
    RestApiClient.performRequest(request, callback);
}

function addCaregiver(caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.post_caregiver , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });
    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_caregiver , {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });
    RestApiClient.performRequest(request, callback);
}

export {
    getCaregivers,
    getCaregiverById,
    updateCaregiver,
    addCaregiver,
    deleteCaregiver
};
