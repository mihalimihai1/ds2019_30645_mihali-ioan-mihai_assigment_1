package com.example.springdemo.repositories;

import com.example.springdemo.entities.Intake;
import com.example.springdemo.entities.Medications;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationsRepository extends JpaRepository<Medications, Integer> {



    @Query(value = "SELECT m " +
            "FROM Medications m " +
            "ORDER BY m.name ")
    List<Medications> getAllOrdered();
}
