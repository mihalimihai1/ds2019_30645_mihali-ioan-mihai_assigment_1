import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_login: '/user/'
};

function getLogin(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_login + params.username +'/'+ params.password, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {getLogin};
