package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.entities.Doctor;

public class DoctorBuilder {


    private DoctorBuilder(){
    }
    public static DoctorDTO generateDTOFromEntity(Doctor doctor) {
        DoctorDTO doctorDTO = new DoctorDTO(
                doctor.getDoctor_id(),
                doctor.getName()
        );
        return  doctorDTO;
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO) {
        return new Doctor(
                doctorDTO.getDoctor_id(),
                doctorDTO.getName()
        );
    }
}
