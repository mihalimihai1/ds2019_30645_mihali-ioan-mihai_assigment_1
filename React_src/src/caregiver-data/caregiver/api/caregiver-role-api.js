import {HOST} from "../../../commons/hosts";
import * as RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_caregiver: '/caregiver/',

};

function getPatientsByCaregiver_id(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregiver + params, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

export {
    getPatientsByCaregiver_id
};
