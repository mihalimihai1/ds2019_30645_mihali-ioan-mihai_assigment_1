package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "doctor")
public class Doctor {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "doctor_id", unique = true, nullable = false)
    private Integer doctor_id;

    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "user_id")
    private User user;

    @OneToMany(mappedBy = "doctor")
    private List<Caregiver> caregiverList;

    private String name;

    public Doctor() {
    }

    public Doctor(Integer doctor_id, String name) {
        this.name = name;
        this.doctor_id = doctor_id;
    }

    public Doctor(String name, User user, List<Caregiver> caregiverList) {
        this.caregiverList = caregiverList;
        this.name = name;
        this.user = user;
    }

    public Doctor(String name) {
        this.name = name;
    }

    public List<Caregiver> getCaregiverList() {
        return caregiverList;
    }

    public void setCaregiverList(List<Caregiver> caregiverList) {
        this.caregiverList = caregiverList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Integer doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
