package com.example.springdemo.controller;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.services.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value="/intake")
public class IntakeController {

    private final IntakeService intakeService;

    @Autowired
    public IntakeController(IntakeService intakeService){
        this.intakeService = intakeService;
    }

    @GetMapping(value ="/{id}")
    public IntakeDTO findById(@PathVariable("id") Integer id){
        return intakeService.findIntakeById(id);
    }

    @GetMapping()
    public List<IntakeDTO> findAll(){
        return intakeService.findAll();
    }

    @PostMapping()
    public Integer insertIntakeDTO(@RequestBody IntakeDTO intakeDTO){
        return intakeService.insert(intakeDTO);
    }

    @PutMapping()
    public Integer updateIntake(@RequestBody IntakeDTO intakeDTO){
        return intakeService.update(intakeDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody IntakeDTO intakeDTO){
        intakeService.delete(intakeDTO);
    }
}
