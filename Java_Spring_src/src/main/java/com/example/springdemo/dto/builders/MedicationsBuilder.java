package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationsDTO;
import com.example.springdemo.entities.Medications;

public class MedicationsBuilder {

    private MedicationsBuilder() {
    }

    public static MedicationsDTO generateDTOFromEntity(Medications medications ){
        return new MedicationsDTO(
                medications.getMedications_id(),
                medications.getName(),
                medications.getList_effects(),
                medications.getDosage()
        );
    }

    public static Medications generateEntityFromDTO(MedicationsDTO medicationsDTO) {
        return new Medications(
                medicationsDTO.getMedications_id(),
                medicationsDTO.getName(),
                medicationsDTO.getList_effects(),
                medicationsDTO.getDosage()
        );
    }
}
