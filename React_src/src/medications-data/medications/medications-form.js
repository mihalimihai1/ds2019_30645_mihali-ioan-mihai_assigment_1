import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medications-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import TextInput from "../../user-data/user/fields/TextInput";


class MedicationsForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {

                medications_id: {
                    value: '',
                    placeholder: 'id',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 1,
                        isRequired: true
                    }

                },

                dosage: {
                    value: '',
                    placeholder: 'dosage',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 1,
                        isRequired: true
                    }
                },

                list_effects: {
                    value: '',
                    placeholder: 'list_effects',
                    valid: false,
                    touched: false,

                },
                name: {
                    value: '',
                    placeholder: 'name',
                    valid: false,
                    touched: false,

                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerMedications(medications){
        return API_USERS.addMedications(medications, (result, status, error) => {

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted medications with id: " + result);

            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updateMedications(medications){
        return API_USERS.updateMedications(medications, (result, status, error) => {

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully updated patient with id: " + result);

            } else {
                this.state.errorStatus = status;
                this.error = error;
            }


        });
    }

    deleteMedications(id){
        return API_USERS.deleteMedications(id, (result, status, error) => {

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully deleted medications with id: " + result);

            } else {
                this.state.errorStatus = status;
                this.error = error;
            }


        });
    }

    handleUpdate(){
        let medications = {
            medications_id: this.state.formControls.medications_id.value,
            dosage : this.state.formControls.dosage.value,
            list_effects: this.state.formControls.list_effects.value,
            name: this.state.formControls.name.value,
        };

        this.updateMedications(medications);
    }

    handleSubmit(){

        let medications = {
            medications_id: this.state.formControls.medications_id.value,
            dosage : this.state.formControls.dosage.value,
            list_effects: this.state.formControls.list_effects.value,
            name: this.state.formControls.name.value,
        };

        this.registerMedications(medications);
    }

    handleDelete(){
        let id = {medications_id : this.state.formControls.medications_id.value};
        this.deleteMedications(id);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>

                <h1>Add new medications</h1>

                <p> medications_id: </p>
                <TextInput name="medications_id"
                           placeholder={this.state.formControls.medications_id.placeholder}
                           value={this.state.formControls.medications_id.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.medications_id.touched}
                           valid={this.state.formControls.medications_id.valid}
                />

                <p> dosage: </p>
                <TextInput name="dosage"
                           placeholder={this.state.formControls.dosage.placeholder}
                           value={this.state.formControls.dosage.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.dosage.touched}
                           valid={this.state.formControls.dosage.valid}
                />

                <p> list_effects: </p>
                <TextInput name="list_effects"
                           placeholder={this.state.formControls.list_effects.placeholder}
                           value={this.state.formControls.list_effects.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.list_effects.touched}
                           valid={this.state.formControls.list_effects.valid}
                />

                <p> name: </p>
                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />

                <p></p>
                <Button variant="success"
                        type={"submit"}
                >
                    Add
                </Button>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

                <p></p>
                <div>
                    <Button
                        variant="success"
                        type={"update"}
                    >
                        Update
                    </Button>
                </div>

                <p></p>
                <div>
                    <Button
                        variant="success"
                        type={"delete"}
                    >
                        Delete
                    </Button>
                </div>

            </form>


        );
    }
}

export default MedicationsForm;
