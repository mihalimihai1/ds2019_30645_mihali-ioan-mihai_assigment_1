import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"

import * as API_CAREGIVERS from "./api/caregiver-api"
import CaregiverForm from "./caregiver-form";

const columns = [
    {
        Header:  'caregiver_id',
        accessor: 'caregiver_id',
    },
    {
        Header: 'address',
        accessor: 'address',
    },
    {
        Header: 'birthdate',
        accessor: 'birthdate',
    },
    {
        Header: 'gender',
        accessor: 'gender',
    },
    {
        Header: 'name',
        accessor: 'name',
    },
];

const filters = [];

class Caregivers extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers() {
        return API_CAREGIVERS.getCaregivers((result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                            caregiver_id:x. caregiver_id,
                            address:x.address,
                            birthdate:x.birthdate,
                            gender:x.gender,
                            name:x.name,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <CaregiverForm
                                    registerCaregiver={this.refresh}
                                    updateCaregiver={this.refresh}>
                                </CaregiverForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Caregivers;
