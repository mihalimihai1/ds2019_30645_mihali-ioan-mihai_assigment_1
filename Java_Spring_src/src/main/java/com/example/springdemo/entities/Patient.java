package com.example.springdemo.entities;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "patient_id", unique = true, nullable = false)
    private Integer patient_id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId", referencedColumnName = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "caregiverId")
    private Caregiver caregiver;

    @OneToOne(mappedBy = "patient")
    private Intake intake;

    @Column(name = "name")
    private String name;

    @Column(name="birthdate")
    private String birthdate;

    @Column(name="gender")
    private String gender;

    @Column(name="address")
    private String address;

    @Column(name="medicalrecord")
    private String medicalrecord;

    public Patient() {
    }

    public Patient(User user, Caregiver caregiver, Intake intake, String name, String birthdate, String gender, String address, String medicalrecord) {
        this.user = user;
        this.caregiver = caregiver;
        this.intake = intake;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalrecord = medicalrecord;
    }

    public Patient(Integer patient_id, String name, String birthdate, String gender, String address, String medicalrecord) {
        this.patient_id = patient_id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalrecord = medicalrecord;
    }

    public Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public Intake getIntake() {
        return intake;
    }

    public void setIntake(Intake intake) {
        this.intake = intake;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalrecord() {
        return medicalrecord;
    }

    public void setMedicalrecord(String medicalrecord) {
        this.medicalrecord = medicalrecord;
    }
}
