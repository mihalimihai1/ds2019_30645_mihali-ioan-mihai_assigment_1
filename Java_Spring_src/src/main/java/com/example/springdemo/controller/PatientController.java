package com.example.springdemo.controller;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.CaregiverService;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value="/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService){
        this.patientService = patientService;
    }

    @GetMapping(value ="/{id}")
    public PatientDTO findById(@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping()
    public List<PatientDTO> findAll(){
        return patientService.findAll();
    }

    @PostMapping()
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @PutMapping()
    public Integer updatePatient(@RequestBody PatientDTO patientDTO){
        return patientService.update(patientDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PatientDTO patientDTO){
        patientService.delete(patientDTO);
    }
}
