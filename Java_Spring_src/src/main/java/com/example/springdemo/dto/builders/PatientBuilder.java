package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Patient;

public class PatientBuilder {
    private PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient) {
        return new PatientDTO(
                patient.getPatient_id(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalrecord()
                );
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
        return new Patient(
                patientDTO.getPatient_id(),
                patientDTO.getName(),
                patientDTO.getBirthdate(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getMedicalrecord()
                );

    }
}
