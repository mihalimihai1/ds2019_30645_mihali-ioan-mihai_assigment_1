package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {


    @Query(value = "SELECT c " +
                    "FROM Caregiver c " +
                    "ORDER BY c.name ")
    List<Caregiver> getAllOrdered();

    @Query(value = "SELECT p FROM Patient p where caregiver_id =?1")
    List<Patient> findPatientsByCaregiver_id(Integer id);
}
