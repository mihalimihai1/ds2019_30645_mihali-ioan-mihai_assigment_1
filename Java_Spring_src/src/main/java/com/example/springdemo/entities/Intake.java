package com.example.springdemo.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "intake")
public class Intake {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "intake_id", unique = true, nullable = false)
    private Integer intake_id;

    @Column(name = "start", length = 100, unique=false, nullable = false)
    private String start;

    @Column(name="end", length = 100,  unique = false, nullable = false)
    private String end;

    @OneToOne
    @JoinColumn(name="patientId", referencedColumnName = "patient_id")
    private Patient patient;

    @OneToOne
    @JoinColumn(name="medicationId", referencedColumnName = "medications_id")
    private Medications medications;

    public Intake() {
    }

    public Intake(Integer intake_id, Medications medications, String start, String end, Patient patient) {
        this.intake_id = intake_id;
        this.start = start;
        this.end = end;
        this.patient = patient;
        this.medications = medications;
    }

    public Intake(Integer intake_id,String start, String end) {
        this.intake_id=intake_id;
        this.start = start;
        this.end = end;
    }

    public Medications getMedications() {
        return medications;
    }

    public void setMedications(Medications medications) {
        this.medications = medications;
    }

    public Integer getIntake_id() {
        return intake_id;
    }

    public void setIntake_id(Integer intake_id) {
        this.intake_id = intake_id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
