package com.example.springdemo.repositories;

import com.example.springdemo.entities.Intake;
import com.example.springdemo.entities.Medications;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IntakeRepository extends JpaRepository<Intake, Integer> {

    @Query(value = "SELECT i " +
            "FROM Intake i " +
            "ORDER BY i.intake_id ")
    List<Intake> getAllOrdered();
}
