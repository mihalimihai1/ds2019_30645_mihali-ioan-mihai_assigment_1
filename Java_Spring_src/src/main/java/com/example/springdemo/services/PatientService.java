package com.example.springdemo.services;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository caregiverRepository){
        this.patientRepository=caregiverRepository;
    }

    public PatientDTO findPatientById(Integer id){

        Optional<Patient> patient  = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();

        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {

        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getPatient_id();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> caregiver = patientRepository.findById(patientDTO.getPatient_id());
        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getPatient_id();
    }

    public void delete(PatientDTO patientDTO){
        this.patientRepository.deleteById(patientDTO.getPatient_id());
    }
}
