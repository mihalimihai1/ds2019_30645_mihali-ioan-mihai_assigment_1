import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

import React, {Component} from 'react';
import './app.css';
import Login from "./user-data/user/login";
import Patients from "./patient-data/patient/patient";
import Caregivers from "./caregiver-data/caregiver/caregiver";
import Medications from "./medications-data/medications/medications";
import Intake from "./intake-data/intake/intake";
import CaregiverRole from "./caregiver-role";

let enums = require('./commons/constants/enums');

class App extends Component {

    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/caregiverrole'
                            render={() => <CaregiverRole/>}
                        />

                        <Route
                            exact
                            path='/intake'
                            render={() => <Intake/>}
                        />

                        <Route
                            exact
                            path='/medications'
                            render={() => <Medications/>}
                        />


                        <Route
                            exact
                            path='/caregiver'
                            render={() => <Caregivers/>}
                        />

                        <Route
                            exact
                            path='/patient'
                            render={() => <Patients/>}
                        />

                        <Route
                            exact
                            path='/'
                            render={() => <Login/>}
                        />

                        <Route
                            exact
                            path='/home'
                            render={() => <Home/>}
                        />

                        */
                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
