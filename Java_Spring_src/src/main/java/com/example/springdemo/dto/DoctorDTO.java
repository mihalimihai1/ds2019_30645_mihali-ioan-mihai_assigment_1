package com.example.springdemo.dto;

import java.util.List;

public class DoctorDTO {
    private Integer doctor_id;
    private List<CaregiverDTO> caregiverList;
    private String name;

    public DoctorDTO(Integer doctor_id, List<CaregiverDTO> caregiverList, String name) {
        this.doctor_id = doctor_id;
        this.caregiverList = caregiverList;
        this.name = name;
    }

    public DoctorDTO(Integer doctor_id, String name) {
        this.doctor_id = doctor_id;
        this.name = name;
    }

    public DoctorDTO() {
    }

    public Integer getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Integer doctor_id) {
        this.doctor_id = doctor_id;
    }

    public List<CaregiverDTO> getCaregiverList() {
        return caregiverList;
    }

    public void setCaregiverList(List<CaregiverDTO> caregiverList) {
        this.caregiverList = caregiverList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
