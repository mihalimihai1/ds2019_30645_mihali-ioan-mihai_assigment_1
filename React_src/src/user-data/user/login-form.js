import React from 'react';
import validate from "./validators/user-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/user-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import Redirect from "react-router-dom/Redirect";

class LoginForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            data:null,
            redirect:false,
            errorStatus: 0,
            error: null,
            formIsValid: false,
            role:'',
            formControls : {

                username: {
                    value: '',
                    placeholder: 'Enter your username...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 100,
                        isRequired: true
                    }
                },

                password: {
                    value: '',
                    placeholder: 'Enter your password...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 100,
                        isRequired: true
                    }
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

 /*   componentDidMount() {
            fetch('http://localhost:8080/user/a/a').then(response=>response.json()).then(data=>this.setState({data}));
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.formControls.username!== this.state.formControls.username){
            fetch(`http://localhost:8080/user/${this.state.formControls.username.value}/${this.state.formControls.password.value}`).then(data=>{data.json()})
                .then((data)=>{console.log(data);this.setState({role:data})})
        }
    }
*/
    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };



    getLogin(user){
      /*  console.log("hello"+ user.username);
        return API_USERS.getLogin(user => {
            console.log("++",user);

           /!* if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully logged in." + result);

            } else {
                this.state.errorStatus = status;
                this.error = error;
            }*!/
        });
*/
      /*  fetch(`http://localhost:8080/user/${user.username}/${user.password}`).then(data=>data.json())
            .then((data)=>{console.log(data);this.setState({role:data})});*/
        }



    handleSubmit=(e)=>{
        e.preventDefault();

        console.log("username: " + this.state.formControls.username.value);
        console.log("password: " + this.state.formControls.password.value);

        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
        };
/*
        this.getLogin(user);*/
        fetch(`http://localhost:8080/user/${user.username}/${user.password}`, {
            headers : {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }

        }).then(data=>data.text())
            .then(data=>{console.log(data);localStorage.setItem('role',data);
            this.setState({redirect:true})});

    }

    render() {
        console.log(this.state.data);
        if(this.state.redirect ===true){
            return <Redirect to='/home'/>
        }
        return (

            <form onSubmit={this.handleSubmit}>

                <h1>Login</h1>

                <p> Username: </p>

                <TextInput name="username"
                           placeholder={this.state.formControls.username.placeholder}
                           value={this.state.formControls.username.value}
                           onChange={this.handleChange}
                />
                {
                <div className={"error-message row"}> * Username must have at least 3 characters </div>}

                <p> Password: </p>
                <TextInput name="password"
                           placeholder={this.state.formControls.password.placeholder}
                           value={this.state.formControls.password.value}
                           onChange={this.handleChange}
                />
                {
                <div className={"error-message"}> * Password must have a valid format</div>}


                <Button variant="success"
                        type={"submit"}
                        >
                    Login
                </Button>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default LoginForm;
