package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder(){
    }
    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver) {
        CaregiverDTO caregiverDTO = new CaregiverDTO(
                caregiver.getCaregiver_id(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getAddress()
        );
        return  caregiverDTO;
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO) {
        return new Caregiver(
                caregiverDTO.getCaregiver_id(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress()
        );
    }
}
